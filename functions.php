<?php
/**
 * Theme setup
 */
function colorban_themesetup(){
    /**
     * require bootstrap navwalker
     */
    require_once get_template_directory().DIRECTORY_SEPARATOR.'includes'.DIRECTORY_SEPARATOR.
        'class-wp-bootstrap-navwalker.php';
    /**
     * add others theme support
     */
    add_editor_style();
    add_theme_support('post-thumbnails');
    /**
     * Deactivate emojis styles
     */
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    /**
     * Registering menus
     */
    register_nav_menus(array(
        'primary' => __('Primary', 'colorban-theme'),
        'footer' => __('Footer', 'colorban-theme')
    ) );
}
add_action('after_setup_theme', 'colorban_themesetup');

/**
 * styles and js
 */
function colorban_addscripts(){
    /**
     * css
     */
    wp_register_style('fonts', 'https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,300;0,500;0,700;1,300;1,500;1,700&display=swap');
    wp_enqueue_style('fonts');
    wp_register_style('bootstrap', get_template_directory_uri().'/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap');
    wp_register_style('parallax', get_template_directory_uri().'/css/paraxify.css');
    wp_enqueue_style('parallax');
    wp_register_style('theme', get_stylesheet_uri());
    wp_enqueue_style('theme');
    /**
     * Js
     */
    wp_enqueue_script('jquery');
    wp_register_script('bootstrap', get_template_directory_uri().'/js/bootstrap.min.js', 'jquery', '4.5.2', true);
    wp_enqueue_script('bootstrap');
    wp_register_script('parallax', get_template_directory_uri().'/js/paraxify.min.js', 'jquery', '0.1', true);
    wp_enqueue_script('parallax');
    wp_register_script('cookies', get_template_directory_uri().'/js/cookies.js', 'jquery', '1.0', true);
    wp_enqueue_script('cookies');
    wp_register_script('scripts', get_template_directory_uri().'/js/scripts.js', 'jquery', '1.0', true);
    wp_enqueue_script('scripts');


}
add_action('wp_enqueue_scripts', 'colorban_addscripts');

/**
 * TODO make a filter and change the _default_wp_die_handler
 */
function colorban_maintenance_mode(){
    if(defined('UNDER_CONSTRUCTION') && (UNDER_CONSTRUCTION === true) && !is_user_logged_in()){
        include_once __DIR__.DIRECTORY_SEPARATOR.'custom-templates'.DIRECTORY_SEPARATOR.
            'maintenance.php';
        http_response_code(503);
        die();
    }
}
add_action('get_header', 'colorban_maintenance_mode');

/**
 * Add page into the body class
 * @param $classes
 * @return mixed
 */
function add_slug_body_class($classes) {
    global $post;
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
    }
    return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );

/**
 * show all post under gallery category in the category gallery view
 * @param $query
 */
function alter_main_query_gallery($query){
    if($query->is_main_query() && is_category(3)){
        $query->set('posts_per_page', '-1');
    }
}
add_action('pre_get_posts', 'alter_main_query_gallery');


/**
 * #####################################################################################################################
 * #------------------------------------ PRIVATE FUNCTIONS FOR THE THEME ----------------------------------------------#
 * #####################################################################################################################
 */
/**
 * Underconstruction
 */


/**
 * Get images templates
 * @param string $file
 * @return string
 */
function colorban_get_image($file) {
    return get_template_directory_uri().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$file;
}

/**
 * @return string
 */
function colorban_principal_menu(){
    $html = '<ul class="navbar-nav ml-auto">';
    foreach(wp_get_nav_menu_items('principal') as $count => $item){
        $html .= '<li class="nav-item">';
        $html .= '<a class="nav-link" href="'.$item->url.'">'.$item->title.'</a>';
        $html .= '</li>';
    }
    $html .= '</ul>';
    return $html;
}

/**
 * @return string
 */
function colorban_footer_menu(){
    $html = '<ul class="list-group list-group-horizontal-sm justify-content-center">';
    foreach(wp_get_nav_menu_items('footer') as $count => $item){
        $html .= '<li class="list-group-item"><a href="'.$item->url.'">'.$item->title.'</a></li>';
    }
    $html .= '</ul>';
    return $html;
}