<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content="<?php bloginfo('description'); ?>"/>
	<link rel="icon" href="<?php echo colorban_get_image('icons/favicon.ico'); ?>">
	<title>
		<?php wp_title('|', true, 'right'); ?>
		<?php bloginfo('name'); ?>
	</title>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<main>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light shadow">
            <div class="container">
                <a class="navbar-brand" href="<?php echo home_url(); ?>">
                    <img src="<?php echo colorban_get_image('logos/logo-main-menu.png'); ?>">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-principal" aria-controls="menu-principal" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="menu-principal">
                    <?php echo colorban_principal_menu(); ?>
                </div>
            </div>
        </nav>
    </header>