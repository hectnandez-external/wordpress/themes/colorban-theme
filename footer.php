        <footer class="blog-footer">
            <div class="container">
                <div class="row justify-content-center footer-container-menu">
                    <div class="col-sm-8 col-md-4">
                        <img class="img-fluid" src="<?php echo colorban_get_image('logos/logo-verde-fondo-transparente.png') ?>">
                    </div>
                </div>
                <div class="row footer-container-menu">
                    <div class="col-sm-12">
                        <?php echo colorban_footer_menu(); ?>
                    </div>
                </div>
                <div class="row footer-container-rrss">
                    <div class="col-sm-12">
                        <a href="https://www.facebook.com/colorbandesign/" target="_blank"><img src="<?php echo colorban_get_image('icons/icon-facebook.png') ?>"/></a>
                        <a href="https://www.linkedin.com/company/colorbandesign/" target="_blank"><img src="<?php echo colorban_get_image('icons/icon-linkedin.png') ?>"/></a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="container">
                <div class="row footer-container-credits">
                    <div class="col-sm-12">
                        <p>Calle alquería de mina, 11, 46200, Paiporta, Valencia</p>
                        <p>&copy; Copyright <?php echo date('Y', strtotime('now')) ?> Colorban. Todos los derechos reservados.</p>
                    </div>
                </div>
            </div>
        </footer>
        </main>
        <div id="cookie-container" class="container-fluid d-none">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <p> Esta web utiliza cookies para obtener datos estadísticos de la navegación de sus usuarios, debes
                        aceptarlas si quieres seguir navegando. Para más información has click
                        <a href="<?php echo get_permalink(get_page_by_path('politica-de-cookies')) ?>">aquí</a>.
                        <button type="button" class="btn btn-sm btn-secondary" onclick="setCookie()">Aceptar</button>
                    </p>
                </div>
            </div>
        </div>
        <?php wp_footer(); ?>
    </body>
</html>