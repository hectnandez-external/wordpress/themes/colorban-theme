<?php get_header(); ?>
    <div class="page-container container">
        <div class="row">
            <div class="col-sm-12">
                <?php if ( post_password_required() ) : ?>
                    <?php echo get_the_password_form(); ?>
                    <p>¿No tienes contraseña?, Registrate <a href="<?php echo get_page_link(237) ?>">aquí</a>.</p>
                <?php else: ?>
                    <?php if ( have_posts() ) : ?>
                        <?php while ( have_posts() ) : ?>
                            <?php the_post(); ?>
                            <?php the_content(); ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>