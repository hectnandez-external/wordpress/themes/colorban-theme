jQuery(function() {
    var cookieName = "colorban_cookie_session";
    if(getCookie(cookieName).length <= 0){
        jQuery('#cookie-container').removeClass('d-none');
    }
});

/**
 * Get cookie site
 * @param cname
 * @returns {string}
 */
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

/**
 * Set cookie site
 */
function setCookie() {
    var d = new Date();
    d.setTime(d.getTime() + (24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = 'colorban_cookie_session' + "=" + 'cookie_site' + ";" + expires + ";path=/";
    jQuery('#cookie-container').addClass('d-none');
}