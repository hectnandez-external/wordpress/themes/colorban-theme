<?php get_header(); ?>
    <div class="page-container container">
        <div class="row">
            <div class="col-sm-12">
                <?php if(have_posts()): ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- title -->
                            <h1 class="gallery-title"><?php single_cat_title('') ?></h1>
                            <!-- description -->
                            <?php if(category_description()): ?>
                                <p class="gallery-description"><?php echo category_description(); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- Categories -->
                    <?php foreach (get_categories(['child_of' => 3, 'order' => 'DESC']) as $category): ?>
                        <div class="row slide-category">
                            <div class="col-sm-12 slide-category-description">
                                <h2>Espejos <?php echo ucfirst($category->name); ?></h2>
                                <?php if(!empty($category->description)): ?>
                                    <p><?php echo $category->description; ?></p>
                                <?php endif; ?>
                            </div>
                            <div class="col-sm-12 slide-category-carousel">
                                <div id="carousel-<?php echo $category->slug ?>" class="carousel slide carousel-fade" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <?php $loopCount = 0; ?>
                                        <?php while(have_posts()): the_post();?>
                                            <?php if(!has_category($category->name, $post->ID)): ?>
                                                <?php continue; ?>
                                            <?php endif; ?>
                                            <div class="carousel-item <?php echo ($loopCount <= 0)? 'active' : ''; ?>">
                                                <img src="<?php echo get_the_post_thumbnail_url() ?>" class="d-block w-100">
<!--                                                <div class="carousel-caption d-none d-md-block">-->
<!--                                                    <h5>--><?php //echo get_the_title(); ?><!--</h5>-->
<!--                                                </div>-->
                                            </div>
                                            <?php $loopCount++; ?>
                                        <?php endwhile; ?>
                                    </div>
                                    <a class="carousel-control-prev" href="#carousel-<?php echo $category->slug ?>" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-<?php echo $category->slug ?>" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p>Sorry, match with your criteria</p>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>