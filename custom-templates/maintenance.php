<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorban - Sitio en mantenimiento">
    <meta name="author" content="Héctor Hernández">
    <title>Colorban - Sitio en mantenimiento</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        a,
        a:focus,
        a:hover {
            color: #fff;
        }
        .btn-secondary,
        .btn-secondary:hover,
        .btn-secondary:focus {
            color: #333;
            text-shadow: none; /* Prevent inheritance from `body` */
            background-color: #fff;
            border: .05rem solid #fff;
        }
        html,
        body {
            height: 100%;
            background-color: #333;
        }
        body {
            display: -ms-flexbox;
            display: flex;
            color: #fff;
            text-shadow: 0 .05rem .1rem rgba(0, 0, 0, .5);
            box-shadow: inset 0 0 5rem rgba(0, 0, 0, .5);
        }
        .cover-container {
            max-width: 42em;
        }
        .masthead {
            margin-bottom: 2rem;
        }
        .masthead-brand {
            margin-bottom: 0;
        }
        .nav-masthead .nav-link {
            padding: .25rem 0;
            font-weight: 700;
            color: rgba(255, 255, 255, .5);
            background-color: transparent;
            border-bottom: .25rem solid transparent;
        }
        .nav-masthead .nav-link:hover,
        .nav-masthead .nav-link:focus {
            border-bottom-color: rgba(255, 255, 255, .25);
        }
        .nav-masthead .nav-link + .nav-link {
            margin-left: 1rem;
        }
        .nav-masthead .active {
            color: #fff;
            border-bottom-color: #fff;
        }
        @media (min-width: 48em) {
            .masthead-brand {
                float: left;
            }
            .nav-masthead {
                float: right;
            }
        }
        .cover {
            padding: 0 1.5rem;
        }
        .cover .btn-lg {
            padding: .75rem 1.25rem;
            font-weight: 700;
        }
    </style>
</head>
<body class="text-center">
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    <header class="masthead mb-auto">
        <div class="inner">
            <img src="<?php echo colorban_get_image('logos/logo-fondo-transparente.svg') ?>">
        </div>
    </header>
    <main role="main" class="inner cover">
        <h1 class="cover-heading">Página en mantenimiento</h1>
        <p class="lead">Pronto estaremos de vuelta.</p>
    </main>
    <footer class="mastfoot mt-auto">
        <div class="inner">
            <p>© Copyright <?php echo date('Y', strtotime('now')); ?> Colorban. Todos los derechos reservados</p>
        </div>
    </footer>
</div>
</body>
</html>
